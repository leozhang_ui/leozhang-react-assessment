import { takeEvery, call, put, cancel } from "redux-saga/effects";
import API from "../api";
import * as actions from "../actions";

function* watchFetchDrone() {
  const { error, data } = yield call(API.fetchDroneLocation);
  if (error) {
    console.log({ error });
    yield put({ type: actions.API_ERROR_DRONE_LOCATION, code: error.code });
    yield cancel();
    return;
  }

  const droneData = data.data;
  if (!droneData) {
    yield put({ type: actions.API_ERROR });
    yield cancel();
    return;
  }

  yield put({
    type: actions.DRONE_DATA_RECEIVED,
    data: droneData[droneData.length - 1]
  });
}

function* watchAppLoad() {
  yield takeEvery(actions.FETCH_DRONE, watchFetchDrone);
}

export default [watchAppLoad];
