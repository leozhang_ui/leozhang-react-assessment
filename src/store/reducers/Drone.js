import * as actions from "../actions";

const initialState = {
  loading: false,
  latitude: null,
  longitude: null
};

const startLoading = state => {
  return { ...state, loading: true };
};

const droneDataRecevied = (state, action) => {
  const { latitude, longitude } = action.data;

  return {
    ...state,
    loading: false,
    latitude,
    longitude
  };
};

const handlers = {
  [actions.FETCH_DRONE]: startLoading,
  [actions.DRONE_DATA_RECEIVED]: droneDataRecevied
};

export default (state = initialState, action) => {
  const handler = handlers[action.type];
  if (typeof handler === "undefined") return state;
  return handler(state, action);
};
