import { Marker } from "react-google-maps";
import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "../store/actions";

class DroneMarker extends Component {
  componentDidMount() {
    this.props.fetchDroneData();
    setInterval(this.props.fetchDroneData, 4000);
  }
  render() {
    return (
      <Marker
        position={{ lat: this.props.latitude, lng: this.props.longitude }}
      />
    );
  }
}

const mapState = state => {
  const { loading, latitude, longitude } = state.drone;
  return {
    loading,
    latitude,
    longitude
  };
};

const mapDispatch = dispatch => ({
  fetchDroneData: () =>
    dispatch({
      type: actions.FETCH_DRONE
    })
});

export default connect(
  mapState,
  mapDispatch
)(DroneMarker);
