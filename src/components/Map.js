import { withScriptjs, withGoogleMap, GoogleMap } from "react-google-maps";
import DroneMarker from "./DroneMarker";
import React from "react";

const Map = () => {
  const RealtimeMap = withScriptjs(
    withGoogleMap(() => (
      <GoogleMap
        defaultZoom={5}
        defaultCenter={{ lat: 29.7604, lng: -95.3698 }}
      >
        <DroneMarker />
      </GoogleMap>
    ))
  );
  return (
    <RealtimeMap
      googleMapURL="https://maps.googleapis.com/maps/api/js?key=AIzaSyAaClvacMF1y_2HGGn7ZZ6t8lqgRN60aMI"
      loadingElement={<div style={{ height: `100%` }} />}
      containerElement={<div style={{ height: `400px` }} />}
      mapElement={<div style={{ height: `100%` }} />}
    />
  );
};

export default Map;
